from django.urls import include,path
from . import views

app_name = 'polls'

urlpatterns = [
    path('', views.PollListView.as_view(), name='poll-list'),
    path('polls/create', views.PollsCreateView.as_view(), name='poll-create'),
    path('polls/update/<slug:id>', views.PollsUpdateView.as_view(), name='poll-update'),
    path('polls/delete/<slug:id>', views.PollsDeleteView.as_view(), name='poll-delete'),
    path('choice/list/<slug:id>', views.ChoiceListView.as_view(), name='choice-list'),
    path('choice/create/<slug:id>', views.ChoiceCreateView.as_view(), name='choice-create'),
    path('choice/update/<slug:id>', views.ChoiceUpdateView.as_view(), name='choice-update'),
    path('choice/delete/<slug:id>', views.ChoiceDeleteView.as_view(), name='choice-delete'),
    # path('vote/update/<slug:id>', views.VoteUpdateView.as_view(), name='vote-update'),
    path('vote/update/<slug:id>', views.vote, name='vote-update'),
]
from django import forms
from .models import Question, Choice

class QuestionModelForm(forms.ModelForm):

    class Meta:
        model = Question
        fields = [
            'question_text',
            'pub_date'
        ]
        widgets = {
            'pub_date': forms.DateTimeInput(format=("%d %b %Y %H:%M:%S %Z"),
                                             attrs={'class': 'form-control', 'placeholder': 'Select a date',
                                                    'type': 'date'}),
        }
class ChoiceModelForm(forms.ModelForm):
    class Meta:
        model =Choice
        fields = [
            'choice_text',
            'votes'
        ]
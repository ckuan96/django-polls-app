from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponseRedirect
from django.views.generic import (
    CreateView,
    DetailView,
    ListView,
    UpdateView,
    DeleteView
)
from django.urls import reverse, reverse_lazy
from django.db.models import Q
from .models import Question, Choice
from .forms import QuestionModelForm, ChoiceModelForm

# Create your views here.
class PollObjectMixin(object):
    model = Question
    lookup = 'id'

    def get_object(self):
        id = self.kwargs.get(self.lookup)
        obj = None
        if id is not None:
            obj = get_object_or_404(self.model, id=id)
        return obj

class ChoiceObjectMixin(object):
    model = Choice
    lookup = 'id'

    def get_object(self):
        id = self.kwargs.get(self.lookup)
        obj = None
        if id is not None:
            obj = get_object_or_404(self.model, id=id)
        return obj

class PollSearchMixin():
    model = None

    def get_queryset(self):
        queryset = self.model.objects.all()
        query = self.request.GET.get('search')

        if query:
            return queryset.filter(Q(question_text__icontains=query))

        return queryset

class PollListView(PollSearchMixin, ListView):
    template_name = 'mypolls/polls_list.html'
    paginate_by = 2
    model = Question


class PollsCreateView(CreateView):
    model =  Question
    form_class = QuestionModelForm
    template_name = 'mypolls/polls_create.html'
    success_url = reverse_lazy('polls:poll-list')

    def form_valid(self, form):
        return super().form_valid(form)


class PollsUpdateView(PollObjectMixin, UpdateView):
    model = Question
    form_class = QuestionModelForm
    template_name = 'mypolls/polls_create.html'
    success_url = reverse_lazy('polls:poll-list')


class PollsDeleteView(PollObjectMixin, DeleteView):
    model = Question
    form_class = QuestionModelForm
    template_name = 'mypolls/polls_delete.html'
    success_url = reverse_lazy('polls:poll-list')


class ChoiceListView(ListView):
    template_name = 'mypolls/choice_list.html'
    model = Choice
    paginate_by = 2

    def get_queryset(self):
        return Choice.objects.filter(question =self.kwargs.get('id'))

class ChoiceCreateView(ChoiceObjectMixin, CreateView):
    model =  Choice
    form_class = ChoiceModelForm
    template_name = 'mypolls/choice_create.html'
    success_url = reverse_lazy('polls:poll-list')


    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.question = Question.objects.get(pk=self.kwargs.get('id'))
        self.object.save()
        return super().form_valid(form)

class ChoiceUpdateView(ChoiceObjectMixin, UpdateView):
    model = Choice
    form_class = ChoiceModelForm
    template_name = 'mypolls/choice_create.html'

    def get_success_url(self):
        data = Choice.objects.get(pk = self.object.pk)
        return reverse('polls:choice-list', args=(data.question.id,))

class ChoiceDeleteView(ChoiceObjectMixin, DeleteView):
    model = Choice
    form_class = ChoiceModelForm
    template_name = 'mypolls/choice_delete.html'

    def get_success_url(self):
        data = Choice.objects.get(pk=self.object.pk).question.id
        return reverse('polls:choice-list', args=(data,))


def vote(request, id):
    data = Choice.objects.get(pk=id)
    data.votes += 1
    data.save()

    return HttpResponseRedirect(reverse('polls:choice-list', args=(data.question.id,)))